from flask import Blueprint, render_template, redirect, url_for, request, flash, abort
from werkzeug.security import generate_password_hash, check_password_hash, safe_str_cmp
from flask_login import login_user, logout_user, login_required
from secrets import token_hex
from datetime import datetime, timedelta
from .models import User
from . import db

auth = Blueprint("auth_patched", __name__)

@auth.route("/login")
def login():
    return render_template("login.html")

@auth.route("/login", methods=["POST"])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(email=email).first()

    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth_patched.login')) 

    login_user(user, remember=remember)
    return redirect(url_for('core_patched.index'))

@auth.route("/signup")
def signup():
    return render_template("signup.html")

@auth.route("/signup", methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')

    user = User.query.filter_by(email=email).first()

    if user:
        flash('Email address already exists')
        return redirect(url_for('auth_patched.signup'))

    new_user = User(email=email, name=name, password=generate_password_hash(password, method="sha256"))

    db.session.add(new_user)
    db.session.commit()

    flash('Sign up successful.')
    logout_user()
    return redirect(url_for('auth_patched.login'))


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('core_patched.index'))

@auth.route("/forgot_password")
def forgot_password():
    return render_template("forgot_password.html")

@auth.route("/forgot_password", methods=["POST"])
def forgot_password_post():
    flash("Please check your email for a reset link.")
    return render_template("forgot_password.html")

def get_secret_reset_string(user):
    secret_reset_string = user.secret_reset_string
    secret_reset_string_creation_time = user.secret_reset_string_creation_time
    now = datetime.now()
    if secret_reset_string and len(secret_reset_string) == 64:
        print("1 ok")
        if secret_reset_string_creation_time is not None:
            print("2 ok")
            print(secret_reset_string_creation_time)
            if now < secret_reset_string_creation_time + timedelta(minutes=5):
                print("3 ok")
                return secret_reset_string
        else:
            print("why is secret reset string creation time none")

    user.secret_reset_string = token_hex(32)
    user.secret_reset_string_creation_time = datetime.now()
    db.session.commit()
    print("secret reset creation time is")
    print(user.secret_reset_string_creation_time)
    return user.secret_reset_string

def verify_secret_reset_string(data_dict):
    secret_reset_string = data_dict.get("secret_reset_string")
    email = data_dict.get("email")
    user = User.query.filter_by(email=email).first()
    if user:
        real_secret_reset_string = get_secret_reset_string(user)
        return safe_str_cmp(secret_reset_string, real_secret_reset_string)

    else:
        return False

@auth.route("/reset_password")
def reset_password():
    if verify_secret_reset_string(request.args):
        email = request.args.get("email")
        secret_reset_string = request.args.get("secret_reset_string")
        return render_template("reset_password.html", email=email, secret_reset_string=secret_reset_string)
    else:
        abort(404)


@auth.route("/reset_password", methods=["POST"])
def reset_password_post():
    if verify_secret_reset_string(request.form):
        new_password = request.form.get("new_password")
        confirm_password = request.form.get("confirm_password")
        if safe_str_cmp(new_password, confirm_password):
            email = request.form.get("email")
            user = User.query.filter_by(email=email).first()
            user.password = generate_password_hash(new_password, method="sha256")
            db.session.commit()
            flash('Reset successful.')
            return render_template("login.html")

        else:
            email = request.form.get("email")
            secret_reset_string = request.form.get("secret_reset_string")
            flash("The passwords must match") 
            return render_template("reset_password.html", email=email, secret_reset_String=secret_reset_string)

    else:
        abort(404)
